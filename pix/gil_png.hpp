#pragma once
#include "pix.hpp"
#include "pix_png.hpp"
#include <boost/gil/gil_all.hpp>

namespace pix {

boost::gil::rgba8_view_t png_view_from_file(char const * fname);

}  // pix
