#include <iostream>
#include <cstdint>
#include "pix/gil_png.hpp"

using std::cout;

char const * bitmap_path = "../assets/lena.png";


int main(int argc, char * argv[])
{
	boost::gil::rgba8_view_t view = pix::png_view_from_file(bitmap_path);

	boost::gil::rgba8_view_t::point_t d = view.dimensions();
	cout << "w:" << d.x << ", h:" << d.y << "\n";
	cout << "size:" << view.size() << "\n";

	return 0;
}
